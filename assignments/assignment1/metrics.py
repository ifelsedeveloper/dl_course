def binary_classification_metrics(prediction, ground_truth):
    '''
    Computes metrics for binary classification

    Arguments:
    prediction, np array of bool (num_samples) - model predictions
    ground_truth, np array of bool (num_samples) - true labels

    Returns:
    precision, recall, accuracy, f1 - classification metrics
    '''
    # TODO: implement metrics!
    # Some helpful links:
    # https://en.wikipedia.org/wiki/Precision_and_recall
    # https://en.wikipedia.org/wiki/F1_score

    tp = 0.0
    tn = 0.0
    fp = 0.0
    fn = 0.0
    for i, predicted_item in enumerate(prediction):
        if predicted_item == ground_truth[i] and ground_truth[i]:
            tp += 1
        if predicted_item == ground_truth[i] and not ground_truth[i]:
            tn += 1
        if predicted_item != ground_truth[i] and ground_truth[i]:
            fn += 1
        if predicted_item != ground_truth[i] and not ground_truth[i]:
            fp += 1

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    accuracy = (tp + tn) / len(ground_truth)
    f1 = 2 * precision * recall / (precision + recall)

    return accuracy, precision, recall, f1


def multiclass_accuracy(prediction, ground_truth):
    '''
    Computes metrics for multiclass classification

    Arguments:
    prediction, np array of int (num_samples) - model predictions
    ground_truth, np array of int (num_samples) - true labels

    Returns:
    accuracy - ratio of accurate predictions to total samples
    '''
    # TODO: Implement computing accuracy
    accurate_predictions = 0.0

    for i, predicted_item in enumerate(prediction):
        if ground_truth[i] == predicted_item:
            accurate_predictions = accurate_predictions + 1
    return accurate_predictions / len(prediction)
